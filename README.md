# iris-api

A simple repository for gitlab CI/CD implementation.


## Branch

```
|-- main
|-- staging
|-- feat/*
```

## Test and Deploy

Fly.io is used for deployment

## Installation

Install dependencies
```
pip install -r requirements.txt
```
