import numpy as np
from src.iris import get_iris_data
from src.distance import calculate_manhattan 

def test_calculate_manhattan():
    dataset = get_iris_data()
    input_data = np.array([[4.9, 3.0, 1.4, 0.2]])
    result = calculate_manhattan(dataset, input_data)
    assert result == 'setosa'